<?php get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<article>
			<div class="wrapper">

				<div class="headline page-title">
					<h1><?php the_title(); ?></h1>
				</div>

				<div class="copy p2">
					<?php the_content(); ?>
				</div>

			</div>
		</article>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>