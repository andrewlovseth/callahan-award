	<?php get_template_part('partials/sponsors'); ?>

	<footer>
		<div class="wrapper">

			<div class="col logo">
				<a href="http://usaultimate.org/" rel="external">
					<img src="<?php $image = get_field('footer_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<div class="col footer-nav">
				<div class="link">
					<a href="<?php echo site_url('/'); ?>">Home</a>
				</div>
				<?php if(have_rows('navigation', 'options')): while(have_rows('navigation', 'options')): the_row(); ?>
					<div class="link">
						<a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>
					</div>
				<?php endwhile; endif; ?>

				<?php if(have_rows('utility_nav', 'options')): while(have_rows('utility_nav', 'options')): the_row(); ?>			 
				    <div class="link">
					    <a href="<?php the_sub_field('link'); ?>">
					        <?php the_sub_field('label'); ?>
					    </a>
					</div>
				<?php endwhile; endif; ?>
			</div>

			<div class="copyright">
				<?php the_field('copyright', 'options'); ?>

				<p class="credits">Site: <a href="http://andrewlovseth.com/">Andrew Lovseth</a></p>
			</div>


		</div>
	</footer>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/plugins.js"></script>
	<script src="<?php bloginfo('template_directory') ?>/js/site.js"></script>
	
	<?php wp_footer(); ?>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-6731123-11"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-6731123-11');
	</script>

</body>
</html>