<?php if( is_singular('wjuc') || is_post_type_archive('wjuc') ): ?>

	<nav id="bar">
		<div class="wrapper">

			<h1><a href="<?php the_field('wjuc_event_homepage', 'options'); ?>"><?php the_field('wjuc_event_name', 'options'); ?></a></h1>

			<?php $posts = get_field('wjuc_event_nav_bar', 'options'); if( $posts !== null ): ?>

				<a href="#" id="menu-toggle">Menu</a>

				<div class="menu">
					<?php get_template_part('partials/bar/menu'); ?>
				</div>

			<?php wp_reset_postdata(); endif; ?>

		</div>
	</nav>

	<nav id="sub-bar">
		<div class="wrapper">

			<?php if(have_rows('wjuc_event_nav', 'options')): while(have_rows('wjuc_event_nav', 'options')): the_row(); ?>

				<?php get_template_part('partials/bar/team-list'); ?>

			<?php endwhile; endif; ?>

		</div>
	</nav>

<?php endif; ?>