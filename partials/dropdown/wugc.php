<div class="col wugc">
	<h4>
		<a href="<?php the_field('wugc_event_homepage', 'options'); ?>">
			<?php the_field('wugc_event_name', 'options'); ?>
		</a>
	</h4>

	<p>
		<?php the_field('wugc_event_location', 'options'); ?><br/>
		<?php the_field('wugc_event_dates', 'options'); ?>
	</p>

	<?php if(have_rows('wugc_event_nav', 'options')): ?>

		<ul>
			<?php while(have_rows('wugc_event_nav', 'options')): the_row(); ?>

				<?php
					$altLabel = get_sub_field('alt_label');
					$divider = get_sub_field('divider');
				?>

				<?php if($divider == true): ?>

					<li class="divider">-</li>

				<?php else: ?>

					<?php $post_object = get_sub_field('link'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>

						<?php if($altLabel): ?>

							<li><a href="<?php the_permalink(); ?>"><?php echo $altLabel; ?></a></li>

						<?php else: ?>

							<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

						<?php endif; ?>

					<?php wp_reset_postdata(); endif; ?>

				<?php endif; ?>

			<?php endwhile; ?>
		</ul>

	<?php endif; ?>
</div>