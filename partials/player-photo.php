<div class="player">
	<div class="image">
		<img src="<?php $image = get_field('headshot'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>

	<h4><?php the_title(); ?></h4>
</div>