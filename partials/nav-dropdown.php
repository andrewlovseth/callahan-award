<nav id="dropdown">
	<div class="wrapper">

		<?php get_template_part('partials/dropdown/world-games'); ?>

		<?php get_template_part('partials/dropdown/wcbu'); ?>

		<?php get_template_part('partials/dropdown/u24'); ?>

		<?php get_template_part('partials/dropdown/wjuc'); ?>

		<?php get_template_part('partials/dropdown/wugc'); ?>

		<div class="col news">
			<h4>
				<a href="<?php echo site_url('/news/'); ?>" class="news">News</a>
			</h4>
		</div>


	</div>
</nav>