<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
	<link rel="preconnect" href="https://fonts.googleapis.com"> 
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin> 
	<link href="https://fonts.googleapis.com/css2?family=Merriweather:ital,wght@0,400;0,700;1,400;1,700&family=Montserrat:wght@400;500;600;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.png" />
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<section id="utility-nav">
		<div class="wrapper">

			<?php if(have_rows('utility_nav', 'options')): while(have_rows('utility_nav', 'options')): the_row(); ?>			 
			    <a href="<?php the_sub_field('link'); ?>">
			        <?php the_sub_field('label'); ?>
			    </a>
			<?php endwhile; endif; ?>

		</div>
	</section>

	<header>
		<div class="wrapper">

			<div class="logo">
				<a href="<?php echo site_url('/'); ?>">
					<img src="<?php $image = get_field('logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</a>
			</div>

			<a href="#" id="toggle" class="no-translate">
				<div class="patty"></div>
			</a>

		</div>
	</header>


	<nav id="bar">
		<div class="wrapper">

			<div class="menu">
				<?php if(have_rows('navigation', 'options')): while(have_rows('navigation', 'options')): the_row(); ?>
					<div class="link">
						<a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>
					</div>
				<?php endwhile; endif; ?>


				<div class="mobile-links">
					<?php if(have_rows('utility_nav', 'options')): while(have_rows('utility_nav', 'options')): the_row(); ?>			 
					    <a href="<?php the_sub_field('link'); ?>">
					        <?php the_sub_field('label'); ?>
					    </a>
					<?php endwhile; endif; ?>
				</div>
			</div>

		</div>
	</nav>