<?php

/*

	Template Name: History

*/

get_header(); ?>

  	<?php get_template_part('partials/hero'); ?>

	<section id="main">
		<div class="wrapper">

			<article class="default">

				<div class="content">
					<?php the_field('content'); ?>
				</div>
			
				<div class="winners">
					<h3>Past Winners</h3>

					<div class="table-header">
					    <div class="year">
					    	<h5>Year</h5>
					    </div>

					    <div class="division mens">
					    	<h5>Men's Division</h5>
					    </div>

					    <div class="division womens">
					    	<h5>Women's Division</h5>
					    </div>
					</div>

					<div class="table-body">
						<?php if(have_rows('history')): while(have_rows('history')): the_row(); ?>

							<div class="entry">
							    <div class="year">
							    	<h4><?php the_sub_field('year'); ?></h4>
							    </div>

							    <div class="division mens">
							    	<h3><?php the_sub_field('mens_winner'); ?></h3>
							    	<p><?php the_sub_field('mens_school'); ?></p>
							    </div>

							    <div class="division womens">
							    	<h3><?php the_sub_field('womens_winner'); ?></h3>
							    	<p><?php the_sub_field('womens_school'); ?></p>
							    </div>
							</div>

						<?php endwhile; endif; ?>						
					</div>
				</div>

			</article>

		</div>
	</section>

<?php get_footer(); ?>