<?php get_header(); ?>

	<?php $division = implode($wp_query->query); ?>

	<section id="hero" class="hero-image" style="background-image: url(<?php $image = get_field($division . 's_hero_image', 'options'); echo $image['url']; ?>);">
		<div class="wrapper">

			<div class="info">
				<h2>
					<span>Callahan Award</span>
				</h2>
				<h1>
					<span><?php the_field($division . 's_hero_headline', 'options'); ?></span>
				</h1>
			</div>

		</div>
	</section>

	<section id="main">
		<div class="wrapper">

			<section id="nominees">

				<div class="header">
					<h2>Nominees</h2>
				</div>

				<?php
					$args = array(
						'post_type' => $division,
						'order' => 'ASC',
						'orderby' => 'title',
						'posts_per_page' => 100,
					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

						<div class="nominee">
							<div class="headshot">
								<a href="<?php the_permalink(); ?>"><img src="<?php $image = get_field('headshot'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" /></a>
							</div>

							<div class="info">
								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?><em><?php the_field('school'); ?></em></a></h3>
							</div>
						</div>

				<?php endwhile; endif; wp_reset_postdata(); ?>

			</section>

		</div>
	</section>

<?php get_footer(); ?>