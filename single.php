<?php get_header(); ?>

    <article>
    	
		<div class="article-header">
	    	<div class="wrapper">
	    	    <h1><?php the_title(); ?></h1>
			</div>
		</div>

		<div class="article-body">
			<div class="wrapper">

				<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
						<?php the_content(); ?>
				<?php endwhile; endif; ?>

		    </div>
	    </div>

    </article>

<?php get_footer(); ?>